package com.alterno;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import java.io.IOException;


/**
 * Created by Alex on 5/22/2018.
 * Project: Alterno
 */
public class ImageEditing extends AppCompatActivity {

    ImageView iv;
    Uri imageUri;
    Bitmap sourceImage;
    Bitmap editedImage;
    double brightnessValue;
    double saturationValue;
    double contrastValue;
    double highlightsValue;
    double shadowsValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_editing);

        iv = findViewById(R.id.editingImage);

        /*
        Get source image and place it in the initial
         */
        if (getIntent().getExtras() != null) {
            imageUri = Uri.parse(getIntent().getStringExtra("imageUri"));
            //iv.setImageURI(imageUri);
            try {
                sourceImage = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                iv.setImageBitmap(sourceImage);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        /*
          Action listeners for the custom seek bar
         */
        final StartPointSeekBar brightnessSeekBar = findViewById(R.id.brightness_seek_bar);
        brightnessSeekBar.setOnSeekBarChangeListener(new StartPointSeekBar.OnSeekBarChangeListener() {
            @Override
            public void onOnSeekBarValueChange(StartPointSeekBar bar, final double value) {
                //Log.d(LOGTAG, "seekbar value:" + value);
                brightnessValue = value;

                Handler uiHandler = new Handler();
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        editedImage = editBrightness(sourceImage, brightnessValue);
                        iv.setImageBitmap(editedImage);
                    }
                };
                uiHandler.postDelayed(runnable, 1);
            }
        });
        brightnessSeekBar.setProgress(0d);

        StartPointSeekBar contrastSeekBar = findViewById(R.id.contrast_seek_bar);
        contrastSeekBar.setOnSeekBarChangeListener(new StartPointSeekBar.OnSeekBarChangeListener() {
            @Override
            public void onOnSeekBarValueChange(StartPointSeekBar bar, double value) {
                //Log.d(LOGTAG, "seekbar value:" + value);
                contrastValue = value;

                Handler uiHandler = new Handler();
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        editedImage = editContrast(sourceImage, contrastValue);
                        iv.setImageBitmap(editedImage);
                    }
                };
                uiHandler.postDelayed(runnable, 1);
            }
        });
        contrastSeekBar.setProgress(0d);

        StartPointSeekBar saturationSeekBar = findViewById(R.id.saturation_seek_bar);
        saturationSeekBar.setOnSeekBarChangeListener(new StartPointSeekBar.OnSeekBarChangeListener() {
            @Override
            public void onOnSeekBarValueChange(StartPointSeekBar bar, double value) {
                //Log.d(LOGTAG, "seekbar value:" + value);
                saturationValue = value / 256;

                Handler uiHandler = new Handler();
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        editedImage = editSaturation(sourceImage, saturationValue);
                        iv.setImageBitmap(editedImage);
                    }
                };
                uiHandler.postDelayed(runnable, 1);
            }
        });
        saturationSeekBar.setProgress(0d);

        StartPointSeekBar highlightsSeekBar = findViewById(R.id.highlights_seek_bar);
        highlightsSeekBar.setOnSeekBarChangeListener(new StartPointSeekBar.OnSeekBarChangeListener() {
            @Override
            public void onOnSeekBarValueChange(StartPointSeekBar bar, double value) {
                //Log.d(LOGTAG, "seekbar value:" + value);
                highlightsValue = value;
            }
        });
        highlightsSeekBar.setProgress(0d);

        final StartPointSeekBar shadowsSeekBar = findViewById(R.id.shadows_seek_bar);
        shadowsSeekBar.setOnSeekBarChangeListener(new StartPointSeekBar.OnSeekBarChangeListener() {
            @Override
            public void onOnSeekBarValueChange(StartPointSeekBar bar, double value) {
                //Log.d(LOGTAG, "seekbar value:" + value);
                shadowsValue = value;
            }
        });
        shadowsSeekBar.setProgress(0d);

    }


    public Bitmap editBrightness(Bitmap src, double value) {
        // original image size
        int width = src.getWidth();
        int height = src.getHeight();

        // create output bitmap
        Bitmap bmOut = Bitmap.createBitmap(width, height, src.getConfig());
        // color information
        int A, R, G, B;
        int pixel;

        // scan through all pixels
        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {
                // get pixel color
                pixel = src.getPixel(x, y);
                A = Color.alpha(pixel);
                R = Color.red(pixel);
                G = Color.green(pixel);
                B = Color.blue(pixel);

                // increase/decrease each channel
                R += value;
                if (R > 255) {
                    R = 255;
                } else if (R < 0) {
                    R = 0;
                }

                G += value;
                if (G > 255) {
                    G = 255;
                } else if (G < 0) {
                    G = 0;
                }

                B += value;
                if (B > 255) {
                    B = 255;
                } else if (B < 0) {
                    B = 0;
                }

                // apply new pixel color to output bitmap
                bmOut.setPixel(x, y, Color.argb(A, R, G, B));
            }
        }

        // return final image
        return bmOut;
    }

    public Bitmap editContrast(Bitmap src, double value) {
        // image size
        int width = src.getWidth();
        int height = src.getHeight();

        // create output bitmap
        Bitmap bmOut = Bitmap.createBitmap(width, height, src.getConfig());

        // color information
        int A, R, G, B;
        int pixel;

        // get contrast value
        double contrast = Math.pow((100 + value) / 100, 2);

        // scan through all pixels
        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {

                // get pixel color
                pixel = src.getPixel(x, y);
                A = Color.alpha(pixel);
                // apply filter contrast for every channel R, G, B
                R = Color.red(pixel);
                R = (int) (((((R / 255.0) - 0.5) * contrast) + 0.5) * 255.0);
                if (R < 0) {
                    R = 0;
                } else if (R > 255) {
                    R = 255;
                }

                G = Color.green(pixel);
                G = (int) (((((G / 255.0) - 0.5) * contrast) + 0.5) * 255.0);
                if (G < 0) {
                    G = 0;
                } else if (G > 255) {
                    G = 255;
                }

                B = Color.blue(pixel);
                B = (int) (((((B / 255.0) - 0.5) * contrast) + 0.5) * 255.0);
                if (B < 0) {
                    B = 0;
                } else if (B > 255) {
                    B = 255;
                }

                // set new pixel color to output bitmap
                bmOut.setPixel(x, y, Color.argb(A, R, G, B));
            }
        }

        // return final image
        return bmOut;
    }

    public Bitmap editSaturation(Bitmap src, double value) {
        // image size
        int width = src.getWidth();
        int height = src.getHeight();


        int[] mapSrcColor = new int[width * height];
        int[] mapDestColor = new int[width * height];

        float[] pixelHSV = new float[3];

        src.getPixels(mapSrcColor, 0, width, 0, 0, width, height);

        int index = 0;
        for (int y = 0; y < height; ++y) {
            for (int x = 0; x < width; ++x) {

                // Convert from Color to HSV
                Color.colorToHSV(mapSrcColor[index], pixelHSV);

                // Adjust HSV
                /*pixelHSV[0] = pixelHSV[0] + settingHue;
                if (pixelHSV[0] < 0.0f) {
                    pixelHSV[0] = 0.0f;
                } else if (pixelHSV[0] > 360.0f) {
                    pixelHSV[0] = 360.0f;
                }*/

                pixelHSV[1] = (float) (pixelHSV[1] + value);
                if (pixelHSV[1] < 0.0f) {
                    pixelHSV[1] = 0.0f;
                } else if (pixelHSV[1] > 1.0f) {
                    pixelHSV[1] = 1.0f;
                }

                /*pixelHSV[2] = pixelHSV[2] + settingVal;
                if (pixelHSV[2] < 0.0f) {
                    pixelHSV[2] = 0.0f;
                } else if (pixelHSV[2] > 1.0f) {
                    pixelHSV[2] = 1.0f;
                }*/

                // Convert back from HSV to Color
                mapDestColor[index] = Color.HSVToColor(pixelHSV);

                index++;
            }
        }

        return Bitmap.createBitmap(mapDestColor, width, height, Bitmap.Config.ARGB_8888);

        // create output bitmap
        //Bitmap bmOut = Bitmap.createBitmap(width, height, src.getConfig());
        //return bmOut;
    }

    public Bitmap editHighlights(Bitmap src, double value) {
        // image size
        int width = src.getWidth();
        int height = src.getHeight();

        // create output bitmap
        Bitmap bmOut = Bitmap.createBitmap(width, height, src.getConfig());

        return bmOut;
    }

    public Bitmap editShadows(Bitmap src, double value) {
        // image size
        int width = src.getWidth();
        int height = src.getHeight();

        // create output bitmap
        Bitmap bmOut = Bitmap.createBitmap(width, height, src.getConfig());

        return bmOut;
    }
}