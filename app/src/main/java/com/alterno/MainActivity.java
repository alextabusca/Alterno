package com.alterno;

import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    private static final int SELECTED_PICTURE = 1;
    private static final int REQUEST_TAKE_PHOTO = 2;

    // Used to load the 'native-lib' library on application startup
    static {
        System.loadLibrary("native-lib");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

    }

    public void selectFromGallery(View v) {
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, SELECTED_PICTURE);
    }

    /*String mCurrentPhotoPath;

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  *//* prefix *//*
                ".jpg",         *//* suffix *//*
                storageDir      *//* directory *//*
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    Uri photoURI;

    public void selectFromCamera(View v) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (photoFile != null) {
                photoURI = FileProvider.getUriForFile(this,
                        "com.alterno.fileprovider",
                        photoFile);
                takePictureIntent.putExtra("imageUri", photoURI.toString());
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    private void handleSmallCameraPhoto(Intent intent) {
        Bundle extras = intent.getExtras();
        assert extras != null;
        Bitmap mImageBitmap = (Bitmap) extras.get("data");
        iv.setImageBitmap(mImageBitmap);
    }*/


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Uri imageUri;

        if (resultCode == RESULT_OK)
            switch (requestCode) {
                case SELECTED_PICTURE:
                    imageUri = data.getData();
                    Intent sendGalleryImageForEditing = new Intent(getApplicationContext(), ImageEditing.class);
                    assert imageUri != null;
                    sendGalleryImageForEditing.putExtra("imageUri", imageUri.toString());
                    startActivity(sendGalleryImageForEditing);
                    break;

                case REQUEST_TAKE_PHOTO:
                    //imageUri = Uri.parse(data.getStringExtra("imageUri"));
                    //Intent sendCameraImageForEditing = new Intent(getApplicationContext(), ImageEditing.class);
                    //assert imageUri != null;
                    //sendCameraImageForEditing.putExtra("imageUri", imageUri);
                    //startActivity(sendCameraImageForEditing);
                    break;
            }
    }
}
